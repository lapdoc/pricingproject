package com.citi.trading.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 */
public class Pricing {

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	
	public static ArrayList<PricePoint> parsePricePoint(ArrayList<String> CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		ArrayList<PricePoint> pricePoints = new ArrayList<PricePoint>();
		
		for(String s: CSV) {
			String[] fields = s.split(",");
			if (fields.length < 6) {
				throw new IllegalArgumentException
					("There must be at least 6 comma-separated fields: '" + CSV + "'");
			}
			
			try {
				Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
				double open = Double.parseDouble(fields[1]);
				double high = Double.parseDouble(fields[2]);
				double low = Double.parseDouble(fields[3]);
				double close = Double.parseDouble(fields[4]);
				int volume = Integer.parseInt(fields[5]);
				
				pricePoints.add(new PricePoint(timestamp, open, high, low, close, volume));
		
			} catch (Exception ex) {
				throw new RuntimeException("Couldn't parse timestamp.", ex);
			}
		}
		return pricePoints;
		
	}
	
	/**
	 * Requests data from the HTTP service and returns it to the caller.
	 */
	public ArrayList<PricePoint> getPriceData(String symbol, int periods) {
		try {
			if(periods > 30 || periods < 1) {
				throw new IllegalArgumentException();
			}
			
			String requestURL = "http://will1.conygre.com:8081/prices/" + symbol + "?periods=" + periods;
			BufferedReader in = new BufferedReader(new InputStreamReader
					(new URL(requestURL).openStream()));
			
			//First line are column names, ignore them
			in.readLine(); // header ... right? No way that could break ...
			
			//Read all pricepoints
			ArrayList<String> arr = new ArrayList<String>();
			for(int i=0; i< periods; i++) {
				String l = in.readLine();
				arr.add(l);
			}
			ArrayList<PricePoint> prices = Pricing.parsePricePoint(arr);
			for(PricePoint p: prices) {
				p.setStock(symbol);
			}
			return prices;
		} catch (IOException ex) {
			throw new RuntimeException
					("Couldn't retrieve price for " + symbol + ".", ex);
		} catch(IllegalArgumentException ex) {
			throw new RuntimeException
					("Periods should between 1 and 30");
		}
	}
	
	/**
	 * Quick test of the component.
	 */
	public static void main(String[] args) {
		Pricing pricing = new Pricing();
		ArrayList<PricePoint> MRKPrice = pricing.getPriceData("MRK", 3);
		for(PricePoint p: MRKPrice) {
			System.out.println(p);
		}
	}
}
